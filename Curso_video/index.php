<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <pre>
            <?php
                require_once 'Pessoa.php';
                require_once 'Livro.php';

                $p[0] = new Pessoa('Erycles', 23, 'M');
                $p[1] = new Pessoa('Mateus', 24, 'M');
                $p[2] = new Pessoa('Luizy', 25, 'F');

                $l[0] = new Livro('PHP Básico','José da Silva', 300, $p[0]);
                $l[1] = new Livro('POO com PHP','Maria de Souza', 500, $p[1]);
                $l[2] = new Livro('PHP Avançado','Ana Paula', 800, $p[2]);
                
                $l[0]->abrir();
                $l[0]->folhear(500);
                $l[1]->folhear(600);
                $l[2]->folhear(900);
                $l[0]->detalhes();
                $l[1]->detalhes();
                $l[2]->detalhes();
            ?>
        </pre>
    </body>
</html>
