<?php 
    class Pessoa{
        private $nome;
        private $idade;
        private $sexo;

        public function fazerAniver(){
            $this->idade ++;
        }

        function __construct($nome, $idade, $sexo){
            $this->nome  = $nome;
            $this->idade = $idade;
            $this->sexo  = $sexo;
        }

        function setNome($nome){
            $this->nome = $nome;
        }

        function getNome(){
            return $this->nome;
        }

        function setIdade($idade){
            $this->idade = $idade;
        }

        function getIdade(){
            return $this->idade;
        }

        function setSexo($sexo){
            $this->sexo = $sexo;
        }

        function getSexo(){
            return $this->sexo;
        }


    }


?>