<?php

class Login{
    private $email;
    private $senha;
    private $nome;

    public function __construct($email, $senha, $nome){
        $this->nome  = $nome;
        $this->setEmail($email);
        $this->setSenha($senha);
    }

    public function getNome(){
        return $this->nome;
    }

    public function Logar(){
        if($this->email == "teste" && $this->senha == "123456"):
            echo "Logado com sucesso!!";
        else:
            echo "Dados inválidos!!";
        endif;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function getSenha(){
        return $this->senha;
    }

    public function setSenha($senha){
        $this->senha = $senha;
    }
}

$logar = new Login("teste", "123456", "Erycles da Silva Santos");
$logar->Logar();
echo $logar->getNome()."<hr>";