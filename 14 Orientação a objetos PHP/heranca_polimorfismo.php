<?php 
    //class mãe ou super classe
    class Felino{
        var $mamifero = 'sim';
        var $correr;

        function correr(){
            echo "correr como felino!";
        }
    }

    //class filha ou subclasse
    class Chita extends Felino{
        //polimorfismo
        function correr(){
            echo "correr como chita 100km/h";
        }
    }
    $chita = new Chita();
    echo $chita->mamifero;
    echo"<br>";
    echo $chita->correr();
?>