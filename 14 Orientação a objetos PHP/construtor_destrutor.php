<?php 
    class Pessoa{
        private $nome;

        public function correr(){
            echo $this->nome . " correndo<br>";
        }

        function __construct($parametro_nome){
            echo "Construtor iniciado<br>";
            $this->nome = $parametro_nome;
            // echo "<br>";
            // echo $this->nome;
        }

        function __destruct(){
            echo "Objeto removido<br>";
            echo "SALAMINHO";
        }
    }

    $pessoa = new Pessoa('Erycles');
    echo $pessoa->correr();
?>