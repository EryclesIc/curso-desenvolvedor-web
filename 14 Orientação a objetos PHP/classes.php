<?php
    class Pessoa{
        //Atributo
        var $nome;

        //getters e setters
        function setNome($nome_definido){
            $this->nome = $nome_definido;
        }

        function getNome(){
            return $this->nome;
        }
    }

    $pessoa = new Pessoa();
    $pessoa->setNome('Erycles'); 
    echo $pessoa->getNome();
    
?>