<?php 
    class Veiculo{
        /*
            Modificadores:
            public (pode ser alterado fora da classe)
            private (não pode ser alterada fora da classe mãe)
            protected (não pode ser alterado fora da classe mãe)
        */
        private $placa;
        private $cor;
        protected $tipo = 'Caminhonete';

        public function setPlaca($num_placa){

            //Validação da placa
            $this->placa = $num_placa;
        }
        public function getPlaca(){
            return $this->placa;
        }
    }

    class Carro extends Veiculo{
        public function exibirTipo(){
            echo $this->tipo;
        }
    }
    $carro = new Carro();
    $carro->exibirTipo();

?>